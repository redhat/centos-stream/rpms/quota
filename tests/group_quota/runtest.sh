#!/bin/bash -x

# Copyright (c) 2010 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Igor ZHANG <yugzhang@redhat.com>

OUTPUTDIR=/tmp
if test ! -d $OUTPUTDIR; then
    echo "creating $OUTPUTDIR"
    mkdir -p $OUTPUTDIR
fi
OUTPUTFILE=`mktemp $OUTPUTDIR/tmp.XXXXXX`

result=PASS
score=0
lo_dev=`losetup -f`
fs_img=/mnt/testarea/fs_img
mnt_point=/mnt/testarea/mnt_point

err_quit() {
    rstrnt-report-result "group_quota" "FAIL" $1
    exit 1
#rhts-abort -t recipe || exit 1
}

setup() {
    mkdir -p /mnt/testarea
    dd if=/dev/null of=$fs_img bs=1G seek=5
    losetup $lo_dev $fs_img
    if [ $? -ne 0 ]; then
        echo "- error: setup loop device" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    mkfs.ext4 $lo_dev
    if [ $? -ne 0 ]; then
        echo "- error: make ext4" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    mkdir -p $mnt_point
    mount -o grpquota $lo_dev $mnt_point
    if [ $? -ne 0 ]; then
        echo "- error: mount ext4 with grpquota" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    chmod 777 $mnt_point && chmod o+t $mnt_point
    if [ $? -ne 0 ]; then
        echo "- error: chmod ext4's root" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    groupadd test
    useradd -g test test1
    useradd -g test test2

    # bug??
    if [ -f $mnt_point/aquota.group.new ]; then
        echo "- error: aquota.group.new is there before quotacheck" | tee -a $OUTPUTFILE
    fi
    # Workaround selinux quota issue, see bug 703871 comment 11 and comment 16
    chcon --reference=/var $mnt_point
    quotacheck -gvd $mnt_point
    if [ -f $mnt_point/aquota.group.new ]; then
        echo "- error: aquota.group.new is there after quotacheck" | tee -a $OUTPUTFILE
        mv $mnt_point/aquota.group.new $mnt_point/aquota.group.broken
    fi
    quotacheck -gvd -a
    if [ $? -ne 0 ]; then
        echo "- error: quotacheck ext4" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    setquota -g test 200000 300000 2000 3000 $mnt_point
    if [ $? -ne 0 ]; then
        echo "- error: setquota for group 'test'" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    li=($(quota -gv test|grep "$lo_dev"|awk '{print $3,$4,$6,$7}'))
    if [ ${li[0]} -ne 200000 -o ${li[1]} -ne 300000 -o ${li[2]} -ne 2000 -o ${li[3]} -ne 3000 ]; then
        echo "- error: setquota not right" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    quotaon -gv $mnt_point
    if [ $? -ne 0 ]; then
        echo "- error: quotaon ext4" | tee -a $OUTPUTFILE
        err_quit 1
    fi
}

do_test() {
    # blocks/soft test
    su - test1 -c "dd if=/dev/zero of=$mnt_point/test1_200m bs=1024 count=200000"
    if [ $? -ne 0 ]; then
        echo "- fail: blocks/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5}'))
    if [ ${t[0]} -ne 200000 -o ${t[1]} -ne 1 ]; then
        echo "- fail: blocks/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    su - test2 -c "dd if=/dev/zero of=$mnt_point/test2_1k bs=1024 count=1"
    if [ $? -ne 0 ]; then
        echo "- fail: blocks/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5,$6}'))
    if [ ${t[0]} != "200004*" -o ${t[1]} != "7days" -o ${t[2]} -ne 2 ]; then
        echo "- fail: blocks/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi


    rm -f /mnt/testarea/mnt_point/{test1_200m,test2_1k}
    # workaround slow quota reaction against deletion on i386/s390x with RHEL6
    # refer to https://bugzilla.redhat.com/show_bug.cgi?id=1403139#c9
    sleep 3s
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5}'))
    if [ ${t[0]} -ne 0 -o ${t[1]} -ne 0 ]; then
        echo "- fail: blocks/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    # blocks/hard test
    su - test1 -c "dd if=/dev/zero of=$mnt_point/test1_300m bs=1024 count=300000"
    if [ $? -ne 0 ]; then
        echo "- fail: blocks/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5,$6}'))
    if [ ${t[0]} != "300000*" -o ${t[1]} != "7days" -o ${t[2]} -ne 1 ]; then
        echo "- fail: blocks/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    su - test2 -c "dd if=/dev/zero of=$mnt_point/test2_1k bs=1024 count=1"
    if [ $? -eq 0 ]; then
        echo "- fail: blocks/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5,$6}'))
    if [ ${t[0]} != "300000*" -o ${t[1]} != "7days" -o ${t[2]} -ne 2 ]; then
        echo "- fail: blocks/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    rm -f /mnt/testarea/mnt_point/{test1_300m,test2_1k}
    # workaround slow quota reaction against deletion on i386/s390x with RHEL6
    # refer to https://bugzilla.redhat.com/show_bug.cgi?id=1403139#c9
    sleep 3s
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $2,$5}'))
    if [ ${t[0]} -ne 0 -o ${t[1]} -ne 0 ]; then
        echo "- fail: blocks/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    # inodes/soft test
    su - test1 -c "n=1; while [[ \$n -le 2000 ]]; do touch $mnt_point/\$n; let n++; done"
    if [ $? -ne 0 ]; then
        echo "- fail: inodes/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5,$8}'))
    if [ ${t[0]} != "2000*" -o ${t[1]} != "7days" ]; then
        echo "- fail: inodes/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    su - test2 -c "touch $mnt_point/test2"
    if [ $? -ne 0 ]; then
        echo "- fail: inodes/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5}'))
    if [ ${t[0]} != "2001*" ]; then
        echo "- fail: inodes/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi


    rm -f /mnt/testarea/mnt_point/{[1-9]*,test2} > /dev/null
    # workaround slow quota reaction against deletion on i386/s390x with RHEL6
    # refer to https://bugzilla.redhat.com/show_bug.cgi?id=1403139#c9
    sleep 3s
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5}'))
    if [ ${t[0]} -ne 0 ]; then
        echo "- fail: inodes/soft test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    # inodes/hard test
    su - test1 -c "n=1; while [[ \$n -le 3000 ]]; do touch $mnt_point/\$n; let n++; done"
    if [ $? -ne 0 ]; then
        echo "- fail: inodes/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5,$8}'))
    if [ ${t[0]} != "3000*" -o ${t[1]} != "7days" ]; then
        echo "- fail: inodes/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi


    su - test2 -c "touch $mnt_point/test2"
    if [ $? -eq 0 ]; then
        echo "- fail: inodes/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5,$8}'))
    if [ ${t[0]} != "3000*" -o ${t[1]} != "7days" ]; then
        echo "- fail: inodes/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi

    rm -f /mnt/testarea/mnt_point/{[1-9]*,test2} > /dev/null
    # workaround slow quota reaction against deletion on i386/s390x with RHEL6
    # refer to https://bugzilla.redhat.com/show_bug.cgi?id=1403139#c9
    sleep 3s
    t=($(quota -vg test|grep "$lo_dev"|awk '{print $5}'))
    if [ ${t[0]} -ne 0 ]; then
        echo "- fail: inodes/hard test" | tee -a $OUTPUTFILE
        err_quit 1
    fi
}

cleanup() {
    userdel -r test1
    userdel -r test2
    groupdel test

    umount $mnt_point
    if [ $? -ne 0 ]; then
        echo "- error: umount ext4" | tee -a $OUTPUTFILE
    fi
    rm -rf $mnt_point > /dev/null
    losetup -d $lo_dev
    if [ $? -ne 0 ]; then
        echo "- error: detach loop device" | tee -a $OUTPUTFILE
    fi
    rm -rf $fs_img > /dev/null
}

# selinux setup to disable RHTS AVC warning.
setup
do_test
cleanup

echo "Silence is gloden" | tee -a $OUTPUTFILE
rstrnt-report-result "group_quota" $result $score
